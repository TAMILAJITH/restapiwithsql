﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.Http;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using RestApiWithSql.Models;


namespace RestApiWithSql.Interfaces
{
    interface StudentInterface
    {
        public IActionResult get(HttpRequest request);
        public IActionResult getSingle(int id, HttpRequest request);
        public IActionResult post(Student data, HttpRequest request);
        public IActionResult put(int id, Student data, HttpRequest request);
        public IActionResult delete(int id, HttpRequest request);





    }
}
