﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace RestApiWithSql.Models
{
    public class StudentDetail
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DateOfBirth { get; set; }
        public string MarksPercentage { get; set; }
    }

    public class ExcelFile
    {
        public IFormFile excelFile { get; set; }
    }
}
