﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace RestApiWithSql.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public int Department { get; set; }
        public DateTime DateOfBirth { get; set; }  
        public int MarksPercentage { get; set; }

    }
}
