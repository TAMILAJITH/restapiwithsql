﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;

namespace RestApiWithSql.Models
{
    public class LogData
    {

        public string Url { get; set; }
        public string Verb { get; set; }
        public ICollection<string> Headers { get; set; }
        public string Query { get; set; }
        public string Param { get; set; }
        public string Time { get; set; }
        public string Date { get; set; }
        public string ExceptionMessage { get; set; }
        public string StackTrace { get; set; }
        public bool RouteToException { get; set; } = false;










    }
}
