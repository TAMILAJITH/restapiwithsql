﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;
using RestApiWithSql.Models;
using System.IO;
using RestApiWithSql.services;





namespace RestApiWithSql.Controllers
{
    [ApiController]
    public class StudentController : ControllerBase
    {

        IConfiguration configure;
        StudentService services;
        public StudentController(IConfiguration configuration,StudentService service)
        {

            configure = configuration;
            services = service;
        }


        

        [Route("api/student/list")]
        [HttpGet]

        public IActionResult get()
        {
            HttpRequest request = Request;

            return services.get(request);

        }


        [Route("api/student/single/{id}")]
        [HttpGet]

        public IActionResult getSingle(int id)
        {
            HttpRequest request = Request;

            return services.getSingle(id,request);

        }

        [Route("api/student/create")]
        [HttpPost]
        public IActionResult post(Student data) 
        {
            HttpRequest request = Request;

            return services.post(data, request);
           

        }

        [Route("api/student/update/{id}")]
        [HttpPost]
        public IActionResult put(int id, Student data)
        {
            HttpRequest request = Request;

            return services.put(id, data, request);


          

        }

        [Route("api/student/delete/{id}")]
        [HttpDelete]

        public IActionResult delete(int id)
        {
            HttpRequest request = Request;

            return services.delete(id, request);


       

        }



    }
}
