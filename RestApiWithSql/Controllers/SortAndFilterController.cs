﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using RestApiWithSql.Models;

namespace RestApiWithSql.Controllers
{
    [Route("api/student/list")]
    [ApiController]
    public class SortAndFilterController : ControllerBase
    {
        IConfiguration configure;

        public SortAndFilterController(IConfiguration configuration)
        {
            configure = configuration;

        }

        [HttpGet("sorting")]

        public IActionResult getSortData(string orderByName,string orderType)
        {
            try {

                if(orderType == "asc" || orderType == "desc")
            {

                }
            else { return Ok("Please enter correct order type"); }

                string query = "select Id,Name,Gender,Department,format(DateOfBirth,'dd/MM/yyyy') as DateOfBirth,MarkPercentage from Students order by " + orderByName + " " + orderType + "";


                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e) {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");
            }

            
          

        }

        [HttpGet("filtering")]

        public IActionResult getFilterData(string filterByName, string filterByValue)
        {

            try {

                string query = "select Id,Name,Gender,Department,format(DateOfBirth,'dd/MM/yyyy') as DateOfBirth,MarkPercentage from Students where " + filterByName + " like '" + filterByValue + "'";


                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");
            }


        }

        [HttpGet("pagination")]

        public IActionResult getDataByPagination(int  pageSize, int pageCount)
        {
            try {
                string query = "select Id,Name,Gender,Department,format(DateOfBirth,'dd/MM/yyyy') as DateOfBirth,MarkPercentage from Students order by Name offset " + (pageCount - 1) * pageSize + " rows fetch next " + pageSize + " rows only";


                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");
            }


          
        }



    }
}
