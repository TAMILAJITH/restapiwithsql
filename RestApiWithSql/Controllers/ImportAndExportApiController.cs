﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestApiWithSql.Models;
using OfficeOpenXml;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Windows;

namespace RestApiWithSql.Controllers
{
    [Route("api/excel")]
    [ApiController]
    public class ImportAndExportApiController : ControllerBase
    {
        IConfiguration configure;
        public ImportAndExportApiController(IConfiguration configuration) { configure = configuration; }

        [HttpPost("import")]
        public IActionResult importToDb([FromForm] ExcelFile excelFile)
        {

            var list = new List<StudentDetail>();
            var stream = new MemoryStream();
            excelFile.excelFile.CopyTo(stream);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(stream);
            var ExcelSheet = package.Workbook.Worksheets[0];
            int rowCount = ExcelSheet.Dimension.Rows;
            for (int i = 2; i <= rowCount; i++)
            {
                list.Add(new StudentDetail
                {
                    Id = ExcelSheet.Cells[i, 1].Value.ToString().Trim(),
                    Name = ExcelSheet.Cells[i, 2].Value.ToString().Trim(),
                    DateOfBirth = ExcelSheet.Cells[i, 3].Value.ToString().Trim(),
                    MarksPercentage = ExcelSheet.Cells[i, 4].Value.ToString().Trim(),
                }); ;
            }

            foreach (StudentDetail item in list)
            {
                DateTime DOB = DateTime.Parse(item.DateOfBirth);
                string query = "insert into StudentDetail values('" + item.Id + "','" + item.Name + "','" + DOB + "','" + item.MarksPercentage + "')";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();
                    }
                }
            }
            return Ok("updated successfully");
        }

        [HttpGet("export")]
        public IActionResult exportData()
        {
            try
            {

                string query = "select * from StudentDetail";
                DataTable table = new DataTable();
                SqlDataReader reader;
                string server = configure.GetConnectionString("Database");
                SqlConnection connection = new SqlConnection(server);
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                DataSet db = new DataSet();
                adapter.Fill(db);
                var stream = new MemoryStream();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage package = new ExcelPackage(stream);
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromDataTable(db.Tables[0], true);
                package.Save();
                stream.Position = 0;
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "StudentData");

            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }

        }


    }
}
