﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestApiWithSql.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace RestApiWithSql.Controllers
{
    [Route("api/file/upload")]
    [ApiController]
    public class FileUploadController : ControllerBase
    {
        IWebHostEnvironment Environment;


        public FileUploadController(IWebHostEnvironment environment) { Environment = environment; }


        [HttpPost]
        public  string fileUpload([FromForm]FileUpload fileData)
        {

            try
            {

                if (fileData.file.Length > 0)
                {
                    string path = Environment.WebRootPath+"\\uploads\\";

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);

                    }

                    FileStream createFile = System.IO.File.Create(path + fileData.file.FileName);
                    fileData.file.CopyTo(createFile);
                    createFile.Flush();
                    return "File upload successfully";
                }
                else { return "File not have any value"; }

            }
            catch (Exception e)
            {
                return e.Message;
            }



        }

    }
}
