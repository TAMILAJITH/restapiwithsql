﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using RestApiWithSql.Models;

namespace RestApiWithSql.Controllers
{
    [Route("api/student")]
    [ApiController]
    public class JoinController : ControllerBase
    {

        IConfiguration configure;
        public JoinController(IConfiguration configuration)
        {

            configure = configuration;
        }

        [HttpGet("joins/list")]
        public IActionResult getJoinData()
        {

            try {
                string query = "select Students.Id,Students.Name,Gender.Name,Departments.Name,Students.DateOfBirth,Students.MarkPercentage from Students ,Gender ,Departments  where Students.Department=Departments.Id and Students.Gender =Gender.Id;";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);


                    return new JsonResult(table);

                }
            }

            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }

           

        
            }

        [HttpGet("between-percentage/all/{from}/{to}")]

        public IActionResult getJoinDataWithPercentage(int from, int to)
        {

            try {
                string query = "select Students.Id,Students.Name,Gender.Name,Departments.Name,Students.DateOfBirth,Students.MarkPercentage from Students ,Gender ,Departments  where Students.Department=Departments.Id and Students.Gender =Gender.Id and MarkPercentage between '" + from + "' and '" + to + "';";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);


                    return new JsonResult(table);

                }
            }

            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }

          
        }

        [HttpGet("by-gender/{genderId}")]

        public IActionResult getDataByGender(int genderId)
        {

            try {
                string query = "select Students.Id,Students.Name,Gender.Name,Departments.Name,Students.DateOfBirth,Students.MarkPercentage from Students join Departments on Students.Department=Departments.Id join Gender on Students.Gender =Gender.Id where Students.Gender='" + genderId + "'";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);


                    return new JsonResult(table);

                }
            }

            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }

          
        }


        [HttpGet("by-department/{departmentId}")]

        public IActionResult getDataByDepartment(int departmentId)
        {

            try {

                string query = "select Students.Id,Students.Name,Gender.Name,Departments.Name,Students.DateOfBirth,Students.MarkPercentage from Students join Departments on Students.Department=Departments.Id join Gender on Students.Gender =Gender.Id where Students.Department='" + departmentId + "'";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);


                    return new JsonResult(table);

                }
            }

            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }

        }

        [HttpGet("count")]

        public IActionResult getStudentCount()
        {

            try {

                string query = "select count(Id) as CountOfStudents from Students";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);


                    return new JsonResult(table);

                }
            }

            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }
           
        }
    }
}
