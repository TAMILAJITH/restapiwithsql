﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using RestApiWithSql.Models;


namespace RestApiWithSql.Controllers
{
    [ApiController]
    public class GenderController : ControllerBase
    {
        IConfiguration configure;

        public GenderController(IConfiguration configuration)
        {

            configure = configuration;
        }

        [Route("api/gender/list")]
        [HttpGet]

        public IActionResult get()
        {

            try {
                string query = "select * from Gender";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch(Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }

           

        }


        [Route("api/gender/single/{id}")]
        [HttpGet]

        public IActionResult getSingle(int id)
        {

            try {

                string query = "select * from Gender where Id ='" + id + "'";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }

        }

        [Route("api/gender/create")]
        [HttpPost]
        public IActionResult post(Gender data)
        {

            try {
                string query = "insert into Gender values('" + data.Id + "','" + data.Name + "')";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }

          
        }

        [Route("api/gender/update/{id}")]
        [HttpPost]
        public IActionResult put(int id, Gender data)
        {

            try {
                string query = "update Gender set Id='" + data.Id + "',Name='" + data.Name + "' where Id ='" + id + "'";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }

           
        }

        [Route("api/gender/delete/{id}")]
        [HttpDelete]

        public IActionResult delete(int id)
        {

            try {
                string query = "delete from Gender where Id ='" + id + "'";
                DataTable table = new DataTable();
                SqlDataReader reader;
                string serverPath = configure.GetConnectionString("Database");
                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();
                    }

                    HttpRequest Req = Request;

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);
                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;

                HttpRequest Req = Request;

                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                return Ok("Exception came");

            }
           
        }

    }
}
