﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestApiWithSql.Models;
using System.IO;


public static class Utils
{

    public static void CreatAndWriteText(LogData data)
    {


        if (!data.RouteToException)
        {
            data.Date = DateTime.Now.ToString("d-MM-yyyy");

            List<LogData> list = new List<LogData>();
            list.Add(data);

            string FileName = @"C:\Users\FTSLAP01\Desktop\c# sample app\RestApiWithSql\RestApiWithSql\TextFiles\Logdetails\api-log-" + data.Date + ".txt";

            if (File.Exists(FileName))
            {
                FileStream fs = new FileStream(FileName, FileMode.Append, FileAccess.Write);
                StreamWriter file = new StreamWriter(fs);
                foreach (var item in list)
                {
                    //file.WriteLine(String.Format("Url = " + item.Url));
                    //file.WriteLine(String.Format("Type = " + item.Type));
                    file.WriteLine("Url = " + item.Url);
                    file.WriteLine("Verb = " + item.Verb);
                    file.WriteLine("Time = " + item.Time);
                    file.WriteLine("Query = " + item.Query);
                    file.WriteLine("Header :");

                    for (int i = 0; i < item.Headers.Count; i++)
                    {
                        file.WriteLine("key = {0}", item.Headers.ElementAt(i));

                    }


                }
                file.WriteLine();
                file.WriteLine();

                file.Close();
            }
            else
            {
                FileStream file = new FileStream(FileName, FileMode.Append, FileAccess.Write);
                StreamWriter createFile = new StreamWriter(file);


                foreach (var item in list)
                {
                    //file.WriteLine(String.Format("Url = " + item.Url));
                    //file.WriteLine(String.Format("Type = " + item.Type));
                    createFile.WriteLine("Url = " + item.Url);
                    createFile.WriteLine("Verb = " + item.Verb);
                    createFile.WriteLine("Time = " + item.Time);
                    createFile.WriteLine("Query = " + item.Query);
                    createFile.WriteLine("Header :");

                    for (int i = 0; i < item.Headers.Count; i++)
                    {
                        createFile.WriteLine("key = {0}", item.Headers.ElementAt(i));

                    }

                    createFile.WriteLine();
                    createFile.WriteLine();
                    createFile.Close();
                }



            }
        }
        else
        {
            data.Date = DateTime.Now.ToString("d-MM-yyyy");

            List<LogData> list = new List<LogData>();
            list.Add(data);

            string FileName = @"C:\Users\FTSLAP01\Desktop\c# sample app\RestApiWithSql\RestApiWithSql\TextFiles\Exceptiondetails\api-exception-" + data.Date + ".txt";

            if (File.Exists(FileName))
            {
                FileStream fs = new FileStream(FileName, FileMode.Append, FileAccess.Write);
                StreamWriter file = new StreamWriter(fs);
                foreach (var item in list)
                {
                    //file.WriteLine(String.Format("Url = " + item.Url));
                    //file.WriteLine(String.Format("Type = " + item.Type));
                    file.WriteLine("Url = " + item.Url);
                    file.WriteLine("Verb = " + item.Verb);
                    file.WriteLine("Time = " + item.Time);
                    file.WriteLine("Query = " + item.Query);
                    file.WriteLine("Header :");

                    for (int i = 0; i < item.Headers.Count; i++)
                    {
                        file.WriteLine("key = {0}", item.Headers.ElementAt(i));

                    }
                    file.WriteLine("ErrorMessage = " + item.ExceptionMessage);
                    file.WriteLine("ErrorStackTrace = " + item.StackTrace);


                }
                file.WriteLine();
                file.WriteLine();

                file.Close();
            }
            else
            {
                FileStream file = new FileStream(FileName, FileMode.Append, FileAccess.Write);
                StreamWriter createFile = new StreamWriter(file);

                foreach (var item in list)
                {
                    //file.WriteLine(String.Format("Url = " + item.Url));
                    //file.WriteLine(String.Format("Type = " + item.Type));
                    createFile.WriteLine("Url = " + item.Url);
                    createFile.WriteLine("Verb = " + item.Verb);
                    createFile.WriteLine("Time = " + item.Time);
                    createFile.WriteLine("Query = " + item.Query);
                    createFile.WriteLine("Header :");

                    for (int i = 0; i < item.Headers.Count; i++)
                    {
                        createFile.WriteLine("key = {0}", item.Headers.ElementAt(i));

                    }
                    createFile.WriteLine("ErrorMessage = " + item.ExceptionMessage);
                    createFile.WriteLine("ErrorStackTrace = " + item.StackTrace);


                    createFile.WriteLine();
                    createFile.WriteLine();
                    createFile.Close();
                }



            }
            data.RouteToException = false;

        }


    }
}

