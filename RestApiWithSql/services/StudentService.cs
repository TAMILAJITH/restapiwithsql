﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestApiWithSql.Models;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Http;
using RestApiWithSql.Interfaces;

namespace RestApiWithSql.services
{
    public class StudentService : StudentInterface
    {

        IConfiguration configure;
        public StudentService(IConfiguration configuration) { configure = configuration; }

        public IActionResult get(HttpRequest Req)
        {
            try
            {

                string query = "select Id,Name,Gender,Department,format(DateOfBirth,'dd/MM/yyyy') as DateOfBirth,MarkPercentage from Students";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }

                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);


                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;


                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);

                throw new Exception("Exception came");
            }



        }

        public IActionResult getSingle(int id, HttpRequest Req)
        {
            try
            {

                string query = "select Id,Name,Gender,Department,format(DateOfBirth,'dd/MM/yyyy') as DateOfBirth,MarkPercentage from Students where Id ='" + id + "'";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }


                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {
                string message = e.Message;
                string stackTrace = e.StackTrace;


                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);

                throw new Exception("Exception came");

            }

        }

        public IActionResult post(Student data, HttpRequest Req)
        {
            try
            {
                string query = "insert into Students values('" + data.Name + "','" + data.Gender + "','" + data.Department + "','" + data.DateOfBirth + "','" + data.MarksPercentage + "')";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }


                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult("posted successfully");

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;


                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                throw new Exception("Exception came");
            }

        }


        public IActionResult put(int id, Student data, HttpRequest Req)
        {
            try
            {

                string query = "update Students set Name='" + data.Name + "',Gender='" + data.Gender + "',Department='" + data.Department + "',DateOfBirth='" + data.DateOfBirth + "',MarkPercentage='" + data.MarksPercentage + "' where Id ='" + id + "'";

                string serverPath = configure.GetConnectionString("Database");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();

                    }



                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;


                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);

                throw new Exception("Exception came");
            }
        }
        public IActionResult delete(int id, HttpRequest Req)
        {
            try
            {
                string query = "delete from Students where Id ='" + id + "'";
                DataTable table = new DataTable();
                SqlDataReader reader;
                string serverPath = configure.GetConnectionString("Database");
                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();
                    }


                    LogData value = new LogData
                    {
                        Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                        Verb = Req.Method,
                        Query = Req.QueryString.ToString(),
                        Time = DateTime.Now.ToString("h:mm tt"),
                        Headers = Req.Headers.Keys

                    };

                    Utils.CreatAndWriteText(value);

                    return new JsonResult(table);

                }
            }
            catch (Exception e)
            {

                string message = e.Message;
                string stackTrace = e.StackTrace;


                LogData value = new LogData
                {
                    Url = $"{Req.Scheme}://{Req.Host.Value}{Req.Path.Value}",
                    Verb = Req.Method,
                    Query = Req.QueryString.ToString(),
                    Time = DateTime.Now.ToString("h:mm tt"),
                    Headers = Req.Headers.Keys,
                    ExceptionMessage = message,
                    StackTrace = stackTrace

                };
                value.RouteToException = true;

                Utils.CreatAndWriteText(value);
                throw new Exception("Exception came");
            }

        }



    }
}
